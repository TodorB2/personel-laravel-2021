<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CallLogsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/store', [HomeController::class, 'store'])->name('store');
Route::get('/logs', [CallLogsController::class, 'listLogs'])->name('logs');
Route::get('/edit/{id}', [CallLogsController::class, 'editLog'])->name('editLog');
Route::get('/delete/{id}', [CallLogsController::class, 'deleteLog'])->name('deleteLog');

Route::put('/update/{id}', [CallLogsController::class, 'updateLog'])->name('updateLog');
Route::get('/users', [UsersController::class, 'listUsers'])->name('list');
Route::get('/user/{id}',[UsersController::class,'userInfo'])->name('userInfo');

