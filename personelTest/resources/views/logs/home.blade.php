@extends('layouts.layout')

@section('content')
    <h3 class="text-center">Logs</h3>
    @if (Session::has('error'))
        <p class="alert {{ Session::get('alert-class', 'alert-danger') }} font-weight-bold col-md-3">
            {{ Session::get('error') }}</p>
    @endif
    @if (Session::has('success'))
        <p class="alert {{ Session::get('alert-class', 'alert-success   ') }} font-weight-bold col-md-3">
            {{ Session::get('success') }}</p>
    @endif
    <div class="row mx-0 my-2">
        <a href="{{ route('home') }}" class="btn btn-success col-md-1 offset-md-9">Home</a>
        <a class="btn btn-primary col-md-1 mx-2" href="{{ route('list') }}">View Users</a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>User</th>
                <th>Client</th>
                <th>Client Type</th>
                <th>Date</th>
                <th>Duration</th>
                <th>Type of Call</th>
                <th>External Call Score</th>
                <th>Actions
                <th>
            </tr>
        </thead>
        <tbody>
            @foreach ($calls as $call)
                <tr>
                    <td>{{ $call->user->name }}</td>
                    <td>{{ $call->client->name }}</td>
                    <td>{{ $call->client->clientType->name }}</td>
                    <td>{{ $call->date }}</td>
                    <td>{{ $call->duration }}</td>
                    <td>{{ $call->callType->name }}</td>
                    <td>{{ $call->external_call_score }}</td>
                    <td> <a href="{{ route('deleteLog', $call->id) }}" class="btn btn-sm btn-danger">Delete</a> 
                         <a href="{{ route('editLog', $call->id) }}" class="btn btn-sm btn-warning">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection


