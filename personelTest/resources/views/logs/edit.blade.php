@extends('layouts.layout')

@section('content')
<div class="container">
    <h3>Edit call log </h3>
    <form action="{{route('updateLog' , $log->id)}}" method="POST" class="form">
        @csrf
        @method("PUT")
        <p>User: </p>
        <div class="form-group">
            <select class="form-control" name="userId">
                @foreach ($users as $user)
                <option value="{{ $user->id }}" @if ($log->user_id == $user->id) selected @endif >{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <p>Client: </p>
            <select class="form-control" name="clientId">
               
                @foreach ($clients as $client)
                    <option value="{{ $client->id }}" @if ($log->client_id == $client->id) selected @endif >{{ $client->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <p>Call Type: </p>
            <select class="form-control" name="callTypeId">
              
                @foreach ($typesOfCall as $type)
                <option value="{{ $type->id }}" @if($log->call_type_id == $type->id) selected @endif>{{$type->name}}</option>
                @endforeach
            </select>
        </div>
        <button class="btn btn-primary">Update</button>
    </form>
</div>
   
@endsection
