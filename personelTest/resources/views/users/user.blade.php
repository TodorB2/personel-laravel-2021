@extends('layouts.layout')

@section('content')
    <h2 class="text-center">Informations</h2>
    <div class="container">
        <div class="row mx-0 text-left">
            <h4 class="col-md-6">Personal informations</h4>
            <div class="col-md-6">
                <a href="{{route('home')}}" class="btn btn-success col-md-3">Home</a>
                <a class="btn btn-warning col-md-3" href="{{route('logs')}}">View Logs</a>
                <a class = "btn btn-success col-md-3" href="{{route('list')}}">Back</a>
            </div>
            <p class="col-md-12">First Name: {{ $firstName }} </p>
            <p class="col-md-12">Last Name: {{ $lastName }} </p>
            <p class="col-md-12">Average Score: {{ round($avgScore, 2) }}</p>
        </div>
        <div class="row mx-0">
            <h4 class="text-center col-md-12">Last 5 calls</h4>
            <table class="table table-striped">
                <thead class="text-center">
                    <tr>
                        <th>User</th>
                        <th>Client</th>
                        <th>Client Type</th>
                        <th>Date</th>
                        <th>Duration</th>
                        <th>Type of Call</th>
                        <th>External Call Score</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($lastFiveCalls as $call)
                        <tr>
                            <td>{{ $call->user->name }}</td>
                            <td>{{ $call->client->name }}</td>
                            <td>{{ $call->client->clientType->name }}</td>
                            <td>{{ $call->date }}</td>
                            <td>{{ $call->duration }}</td>
                            <td>{{ $call->callType->name }}</td>
                            <td>{{ $call->external_call_score }}</td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>






@endsection
