@extends('layouts.layout')

@section('content')
<h3 class="text-center">Users</h3>
@if(Session::has('error'))
<p class="alert {{ Session::get('alert-class', 'alert-danger') }} font-weight-bold col-md-3">{{ Session::get('error') }}</p>
@endif
@if(Session::has('success'))
<p class="alert {{ Session::get('alert-class', 'alert-success   ') }} font-weight-bold col-md-3">{{ Session::get('success') }}</p>
@endif
<a href="{{route('home')}}" class="btn btn-primary col-md-1 offset-md-10 mb-2">Home</a>
<a href="{{route('logs')}}" class="btn btn-warning col-md-1 offset-md-10 mb-2">Call Logs</a>
<div class="container ">
    <table class="table table-bordered">
        <thead >
            <tr class="row mx-0">
                <th class="col-md-10">User</th>
                <th class="col-md-2 text-center">Actions</th>
            </tr>
        </thead>
        <tbody >
            @foreach ($users as $user)
                <tr class="row mx-0">
                    <td class="col-md-10">{{$user->name}}</td>
                    <td class="col-md-2 text-center"><a href="{{route('userInfo', $user->id)}}" class="btn btn-sm btn-success">View</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection