@extends('layouts.layout')

@section('content')
    <div class="container text-center">
        @if (Session::has('error'))
            <p class="alert {{ Session::get('alert-class', 'alert-danger') }} font-weight-bold ">
                {{ Session::get('error') }}</p>
        @endif
       
        <h3>Insert your ".csv" file</h3>
        <a class="btn btn-warning"   href="@if (empty($calls)){{ route('logs') }}@endif" >View Logs</a>
        <a class="btn btn-success"   href="@if (empty($calls)){{ route('list') }}@endif" >View Users</a>
        <form action="  {{ route('store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="form-group row mx-0 p-5">
                <input type="hidden" name="token" value="{{ csrf_token() }}">
                <input type="file" name="fileInput" id="file" class="form-control-file col-md-4 offset-md-4">

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection
