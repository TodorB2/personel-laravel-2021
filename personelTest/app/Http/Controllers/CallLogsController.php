<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Call;
use App\Models\CallType;
use App\Models\ClientType;
use App\Models\Client;
use App\Models\User;

class CallLogsController extends Controller
{
    public function listLogs()
    {
        // $calls = Call::all()->where('duration', '>', 10);
        $calls = Call::with(['user', 'callType', 'client.clientType'])->get();
       
    
        return view('logs.home', compact('calls'));
    }
    public function editLog($id){
        $log = Call::find($id);
        
        $users = User::all();
        $clients = Client::all();
        $typesOfCall =CallType::all();
       

        return view('logs.edit', compact('log', 'users', 'clients', 'typesOfCall'));
    }
    public function deleteLog($id){
        $log = Call::where('id', $id);
        if($log->delete()){
            return redirect()->route('logs')->with('success', 'Successfully deleted!');
        }else{
            return redirect()->route('logs')->with('error', 'The log is not deleted!');
        }
    }
    public function updateLog(Request $request, $id){
       
        $log = Call::find($id);
        $log->user_id = $request->userId;
        $log->client_id = $request->clientId;
        $log->call_type_id = $request->callTypeId;
        
        if($log->save()){
            return redirect()->route('logs')->with('success', 'Edit successfull!');
        }else{
            return redirect()->route('logs')->with('error', 'There was an error!');
        }
    }
}
