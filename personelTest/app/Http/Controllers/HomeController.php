<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Call;
use App\Models\CallType;
use App\Models\ClientType;
use App\Models\Client;
use App\Models\User;
use DateTime;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function store(Request $request)
    {
        $requestFile = $request->fileInput;
       
        if (isset($requestFile) && !empty($requestFile)) {
            $file = file_get_contents($requestFile);
            $fileArr = explode("\n", $file);
            for ($i = 1; $i < count($fileArr) - 1; $i++) {
                $informations = explode(",", $fileArr[$i]);
                $data = str_replace('"', '', $informations);
                if (count($data) >0) {
                    $user = User::firstOrCreate(['name' => $data[0]]);
                    $user->save();
                    $clientType = ClientType::firstOrCreate(['name' => $data[2]]);
                    $clientType->save();
                    $client = Client::firstOrCreate(['name' => $data[1], 'client_type_id' => $clientType->id]);
                    $client->save();
                    $callType = CallType::firstOrCreate(['name' => $data[5]]);
                    $callType->save();
                    $call = Call::firstOrCreate([
                        'user_id' => $user->id, 'client_id' => $client->id, 'date' => date('Y-m-d h:i:s', strtotime($data[3])),
                        'duration' => $data[4], 'call_type_id' => $callType->id, 'external_call_score' => $data[6]
                    ]);
                    $call->save();
                    set_time_limit(120);
                }
            }
            return redirect()->route('logs')->with('success', 'Succesfully updated!');
        }else{
            return redirect()->route('home')->with('error', 'Please update .csv file!');
        }
    }
    
    
}
