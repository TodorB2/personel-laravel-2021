<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Call;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function listUsers()
    {
        $users = User::all();
        return view('users.list', compact('users'));
    }
    public function userInfo($id){
        $user = User::find($id);
        $userCredentials = explode(' ', $user->name);
        $firstName = $userCredentials[0];
        $lastName = $userCredentials[1];
        // $yesterdayCases = Cases::select(DB::raw('SUM(deaths) as yd,SUM(active) as ya,SUM(recovered) as yr, SUM(confirmed) as yc'))->where('date', $dateTo['date'])->get();
        $avgScoreQuery = Call::select(DB::raw('AVG(external_call_score) as ecs'))->where('user_id',$id)->get();
        $avgScore = $avgScoreQuery[0]->ecs;
        $lastFiveCalls = Call::where('user_id',$id)->where('duration', '>', 10)->orderByDesc('date')->limit(5)->get();
         return view('users.user', compact('firstName', 'lastName', 'avgScore', 'lastFiveCalls'));
    }
}
