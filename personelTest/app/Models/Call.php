<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use HasFactory;


    protected $fillable = [
        'user_id','client_id', 'date', 'duration',
         'call_type_id','external_call_score',
    ];

    public function callType(){
        return $this->belongsTo(CallType::class, 'call_type_id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function client(){
        return $this->belongsTo(Client::class, 'client_id');
    }
    

}
