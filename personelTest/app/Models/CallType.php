<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallType extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
    ];

    public function call()
    {
        return $this->hasMany(Call::class, 'call_type_id');
    }
}
