<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    use HasFactory;


    protected $fillable = [
        'name','client_type_id',
    ];

    public function client()
    {
        return $this->hasMany(Client::class, 'client_type_id');
    }
    
}
