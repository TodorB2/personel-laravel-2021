<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'client_type_id',
    ];

    public function call()
    {
        return $this->hasMany(Call::class, 'client_id');
    }
    public function clientType(){
        return $this->belongsTo(ClientType::class, 'client_type_id');
    }
}
